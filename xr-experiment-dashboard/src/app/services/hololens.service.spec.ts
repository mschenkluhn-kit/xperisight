import { TestBed } from '@angular/core/testing';

import { HololensService } from './hololens.service';

describe('HololensService', () => {
  let service: HololensService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HololensService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
