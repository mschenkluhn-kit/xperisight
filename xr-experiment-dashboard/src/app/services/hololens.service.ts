import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HololensService {

  serverUrl: string = environment.serverUrl;

  constructor(private httpService: HttpClient) {
  }

  getBattery(participantId: string, hololensIp: string) {
    return new Observable<number>(observer => {
      this.httpService.get(this.serverUrl + "/participant/" + participantId + "/hololens/" + hololensIp + "/battery").subscribe((response: any) => {
        if (response && response.MaximumCapacity && response.MaximumCapacity) {
          observer.next(Math.min(response.RemainingCapacity / response.MaximumCapacity, 1));
        } else {
          observer.error();
        }
      }, () => {
        observer.error();
      });
    });
  }

  getThermal(participantId: string, hololensIp: string) {
    return new Observable<number>(observer => {
      this.httpService.get(this.serverUrl + "/participant/" + participantId + "/hololens/" + hololensIp + "/thermal").subscribe((response: any) => {
        if (response && response.CurrentStage !== undefined) {
          observer.next(response.CurrentStage);
        } else {
          observer.error();
        }
      }, () => {
        observer.error();
      });
    });
  }
}
