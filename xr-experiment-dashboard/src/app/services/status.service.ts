import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DeviceStatus} from '../models/device-status';
import {Observable, Subject} from 'rxjs';
import {Participant} from '../models/participant';
import {ExperimentStatus} from '../models/experiment-status';
import {EyeTrackerCalibrationStatus} from '../models/eye-tracker-calibration-status';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  serverUrl: string = environment.serverUrl;
  status: Map<string, DeviceStatus> = new Map<string, DeviceStatus>();

  serverOnline: Subject<boolean>;
  participantIds: Subject<Array<string>>;

  constructor(private httpService: HttpClient) {
    this.serverOnline = new Subject<boolean>();
    this.participantIds = new Subject<Array<string>>();

    this.getHealthLoop();
    this.getParticipantIdsLoop();
  }

  getParticipantStatus(participantId: string): Observable<Participant> {
    return new Observable((observer) => {
      this.httpService.get(this.serverUrl + "/participant/" + participantId).subscribe((participantJson: any) => {
        let participant = participantJson as Participant;
        participant.experimentStatus = participant.experimentStatus as ExperimentStatus;
        participant.eyeTrackerCalibrationStatus = participant.eyeTrackerCalibrationStatus as EyeTrackerCalibrationStatus;

        observer.next(participant);
      }, () => {
        observer.error();
      });
    })
  }

  deleteParticipant(participantId: string) {
    this.httpService.delete(this.serverUrl + "/participant/" + participantId).subscribe();
  }

  postInstruction(participantId: string, call: string, parameters: string[] = []) {
    const request_body = {
      'instructionId': 0, 'call': call, 'parameters': parameters
    }
    this.httpService.post(this.serverUrl + "/participant/" + participantId + "/instructions", request_body).subscribe();
  }

  private getHealthLoop() {
    this.httpService.get(this.serverUrl + "/health").subscribe(() => {
      this.serverOnline.next(true);
    }, error => {
      this.serverOnline.next(false);
    });
    setTimeout(() => {
      this.getHealthLoop()
    }, 5000)
  }

  private getParticipantIdsLoop() {
    this.httpService.get(this.serverUrl + "/participant").subscribe((participantIds: any) => {
      const array = participantIds.participantIds as Array<string>;

      this.participantIds.next(array);
    }, () => {
    });

    setTimeout(() => {
      this.getParticipantIdsLoop()
    }, 1000)
  }

}
