import {Component, OnInit} from '@angular/core';
import {StatusService} from './services/status.service';
import {DeviceStatus} from './models/device-status';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'arkb-experiment-dashboard';
  status: DeviceStatus[] = [];
  participantIds: Array<string> = new Array<string>();
  serverConnection: boolean = false;

  constructor(private statusService: StatusService) {
  }

  ngOnInit(): void {
    this.statusService.serverOnline.subscribe(update => this.serverConnection = update);
    this.statusService.participantIds.subscribe(ids => this.participantIds = ids);
  }

}
