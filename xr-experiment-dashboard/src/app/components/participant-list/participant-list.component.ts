import {Component, OnInit} from '@angular/core';
import {StatusService} from '../../services/status.service';

@Component({
  selector: 'app-participant-list',
  templateUrl: './participant-list.component.html',
  styleUrls: ['./participant-list.component.css']
})
export class ParticipantListComponent implements OnInit {

  participantIds: Array<string> = new Array<string>();

  constructor(private statusService: StatusService) {
  }

  ngOnInit(): void {
    this.statusService.participantIds.subscribe(ids => {
      this.participantIds = ids;
    });
  }

}
