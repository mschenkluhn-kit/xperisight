import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Participant} from '../../models/participant';
import {HololensService} from '../../services/hololens.service';
import {StatusService} from '../../services/status.service';

@Component({
  selector: 'app-experiment-info',
  templateUrl: './experiment-info.component.html',
  styleUrls: ['./experiment-info.component.css']
})
export class ExperimentInfoComponent implements OnInit, OnDestroy {
  @Input() participant: Participant = new Participant();

  constructor(private hololensService: HololensService, private statusService: StatusService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  closeParticipant() {
    if (this.participant && this.participant.participantId) {
      this.participant.visible = false;
      this.statusService.deleteParticipant(this.participant.participantId);
    }
  }

  openDevicePortal() {
    window.open("http://" + this.participant.experimentStatus.deviceIp, "_blank");
  }
}
