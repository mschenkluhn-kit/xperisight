import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {StatusService} from '../../services/status.service';
import {Participant} from '../../models/participant';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.css']
})
export class InfoCardComponent implements OnInit, OnDestroy {

  @Input() participantId: string = "";
  participant: Participant = new Participant();

  private alive = true;

  constructor(private statusService: StatusService) {
  }

  ngOnInit(): void {
    this.update();
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  update(): void {
    this.statusService.getParticipantStatus(this.participantId).subscribe(p => {
      this.participant = p;
    });

    setTimeout(() => {
      if (this.alive)
        this.update()
    }, 1000)
  }

}
