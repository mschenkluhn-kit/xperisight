import {Component, Input, OnInit} from '@angular/core';
import {Participant} from '../../models/participant';
import {StatusService} from '../../services/status.service';

@Component({
  selector: 'app-eye-tracking-info',
  templateUrl: './eye-tracking-info.component.html',
  styleUrls: ['./eye-tracking-info.component.css']
})
export class EyeTrackingInfoComponent implements OnInit {
  @Input() participant: Participant = new Participant();

  constructor(private statusService: StatusService) { }

  ngOnInit(): void {
  }

  recalibrateEyeTracker() {
    if (this.participant && this.participant.participantId) {
      this.statusService.postInstruction(this.participant.participantId, 'run_eye_calibration', []);
    }
  }

  openEyeTrackingPrivacySettings() {
    if (this.participant && this.participant.participantId) {
      this.statusService.postInstruction(this.participant.participantId, 'eye_tracking_privacy_settings', []);
    }
  }

}
