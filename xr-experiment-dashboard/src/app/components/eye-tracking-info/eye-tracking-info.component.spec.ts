import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EyeTrackingInfoComponent } from './eye-tracking-info.component';

describe('EyeTrackingInfoComponent', () => {
  let component: EyeTrackingInfoComponent;
  let fixture: ComponentFixture<EyeTrackingInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EyeTrackingInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EyeTrackingInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
