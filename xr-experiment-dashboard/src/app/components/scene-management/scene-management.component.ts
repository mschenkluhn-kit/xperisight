import {Component, Input, OnInit} from '@angular/core';
import {Participant} from '../../models/participant';
import {StatusService} from '../../services/status.service';

@Component({
  selector: 'app-scene-management',
  templateUrl: './scene-management.component.html',
  styleUrls: ['./scene-management.component.css']
})
export class SceneManagementComponent implements OnInit {
  @Input() participant: Participant = new Participant();

  loading = false;
  sceneInput = "";

  constructor(private statusService: StatusService) { }

  ngOnInit(): void {
  }

  loadScene(scene: string): void {
    this.loadingAnimation(2000);

    if (this.participant && this.participant.participantId) {
      this.statusService.postInstruction(this.participant.participantId, 'load_scene', [scene]);
    }

    // Deselect input
    document.getElementsByName("sceneInput")[0].blur();

    setTimeout(() => this.sceneInput = "", 800);
  }

  reloadScene(): void {
    if (this.participant && this.participant.experimentStatus.scene) {
      this.loadScene(this.participant.experimentStatus.scene);
    }
  }

  loadPreviousScene(): void {
    this.loadingAnimation(2000);

    if (this.participant && this.participant.participantId) {
      this.statusService.postInstruction(this.participant.participantId, 'load_previous_scene');
    }
  }

  loadNextScene(): void {
    this.loadingAnimation(2000);

    if (this.participant && this.participant.participantId) {
      this.statusService.postInstruction(this.participant.participantId, 'load_next_scene');
    }
  }

  private loadingAnimation(duration: number) {
    this.loading = true;
    setTimeout(() => this.loading = false, duration);
  }

}
