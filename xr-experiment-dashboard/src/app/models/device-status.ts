export class DeviceStatus {
  DeviceName: string | undefined;
  DeviceModel: string | undefined;
  DeviceType: string | undefined;
  BatteryLevel: number | undefined;
  BatteryStatus: string | undefined;
  CurrentScene: string | undefined;
  RunTime: number | undefined;
  IsMuted: boolean | undefined;
  fps: number | undefined;

  constructor() {
  }
}
