export class ExperimentStatus {
  participantId: string | undefined;
  deviceName: string | undefined;
  deviceModel: string | undefined;
  deviceType: string | undefined;
  deviceIp: string | undefined;
  deviceBatteryLevel: number | undefined;
  deviceBatteryStatus: string | undefined;
  sceneNames: string[] | undefined;
  scene: string | undefined;
  timeInCurrentSceneInSeconds: number = 0;
  fps: number | undefined;
  needsHelp: boolean = false;
  useEyeTracking: boolean | undefined;
}
