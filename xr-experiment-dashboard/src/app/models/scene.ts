export class Scene {
  name: string;
  scene: string;

  constructor(name: string, scene: string) {
    this.name = name;
    this.scene = scene;
  }
}
