import {ExperimentStatus} from './experiment-status';
import {EyeTrackerCalibrationStatus} from './eye-tracker-calibration-status';

export class Participant {
  participantId: string | undefined;
  experimentStatus: ExperimentStatus = new ExperimentStatus();
  eyeTrackerCalibrationStatus: EyeTrackerCalibrationStatus | undefined;
  online: boolean | undefined;
  visible: boolean | undefined;
  last_seen: string | undefined;
}
