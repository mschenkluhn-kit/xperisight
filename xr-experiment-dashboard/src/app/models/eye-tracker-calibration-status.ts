export class EyeTrackerCalibrationStatus {
  timestamp: number | undefined;
  offsetX: number | undefined;
  offsetY: number | undefined;
  isEyeCalibrationValid: boolean | undefined;
  isEyeTrackingEnabledAndValid: boolean | undefined;
  sourceName: string | undefined;
  sourceType: string | undefined;
}
