class EyeTrackerCalibrationStatus:
    timestamp: float
    offsetX: float
    offsetY: float
    isEyeCalibrationValid: bool
    isEyeTrackingEnabledAndValid: bool
    sourceName: str
    sourceType: str

    def __init__(self, my_dict: dict = None):
        if my_dict is not None:
            for key in my_dict:
                setattr(self, key, my_dict[key])
