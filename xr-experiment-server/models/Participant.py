import datetime

from models.ExperimentStatus import ExperimentStatus
from models.EyeTrackerCalibrationStatus import EyeTrackerCalibrationStatus


class Participant:
    participantId: int
    online: bool
    visible: bool
    last_seen: datetime.datetime
    experimentStatus: ExperimentStatus
    eyeTrackerCalibrationStatus: EyeTrackerCalibrationStatus

    def __init__(self, my_dict: dict = None):
        if my_dict is not None:
            for key in my_dict:
                setattr(self, key, my_dict[key])
