class ExperimentStatus:
    participantId: int
    deviceName: str
    deviceModel: str
    deviceType: str
    deviceBatteryLevel: float
    deviceBatteryStatus: str
    deviceIp: str
    sceneNames: list[str]
    scene: str
    needsHelp: bool
    timeInCurrentSceneInSeconds: float
    heatLevel: int
    fps: float
    useEyeTracking: bool

    def __init__(self, my_dict: dict = None):
        if my_dict is not None:
            for key in my_dict:
                setattr(self, key, my_dict[key])
