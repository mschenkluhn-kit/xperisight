# Xperisight Server

## Installation
* Install Python, e.g., Miniconda
* Create a Python environment using your prefered solution, e.g., conda (included in Miniconda)
* Install the requirements. For pip: ```pip install -r requirements.txt```
* Start the applicatoin: ```python app.py```