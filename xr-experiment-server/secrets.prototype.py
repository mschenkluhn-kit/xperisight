from requests.auth import HTTPBasicAuth


# Create secrets.py file with correct credentials
class Secrets:
    hololensCredentials = HTTPBasicAuth('username', 'password')
