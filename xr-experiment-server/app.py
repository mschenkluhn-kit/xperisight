import datetime
import logging

import jsonpickle
import requests
from flask import Flask, request, jsonify, Response, redirect, url_for
from flask_cors import CORS
from gevent.pywsgi import WSGIServer

import secrets
from models.ExperimentStatus import ExperimentStatus
from models.EyeTrackerCalibrationStatus import EyeTrackerCalibrationStatus
from models.Participant import Participant

app = Flask(__name__, static_url_path='')
CORS(app)

data = dict()
instructions = dict()
currentInstructionId = 0

experimentUsesEyeTracking = False


@app.route('/', methods=['GET'])
def serve_webapp():
    return redirect(url_for('static', filename='index.html'))


@app.route('/health', methods=['GET'])
def get_health():
    return jsonify({"online": True, "server-info": "ARKB Experiment Server"})


@app.route('/participant', methods=['GET'])
def get_participants():
    check_online_status()
    ids = [i for i in data.keys() if data[i].visible]

    return jsonify({
        "participantIds": ids
    })


@app.route('/participant/<participantId>', methods=['GET'])
def get_participant(participantId):
    if participantId in list(data.keys()):
        return response(data[participantId])
    else:
        return error_not_found()


@app.route('/participant/<participantId>/experiment-status', methods=['POST'])
def update_state(participantId):
    p = get_or_generate_participant(participantId)
    p.experimentStatus = to_class(request.json, ExperimentStatus)
    p.experimentStatus.useEyeTracking = experimentUsesEyeTracking
    participant_seen(participantId)

    return response_success()


@app.route('/participant/<participantId>/eye-tracker-calibration-status', methods=['POST'])
def update_eye_tracker(participantId):
    global experimentUsesEyeTracking
    experimentUsesEyeTracking = True
    p = get_or_generate_participant(participantId)
    p.eyeTrackerCalibrationStatus = to_class(request.json, EyeTrackerCalibrationStatus)
    participant_seen(participantId)

    return response_success()


@app.route('/participant/<participantId>/instructions', methods=['GET'])
def get_instructions(participantId):
    if participantId in list(instructions.keys()):
        return response(instructions[participantId])
    else:
        return error_not_found()


@app.route('/participant/<participantId>/instructions', methods=['POST'])
def update_instructions(participantId):
    global currentInstructionId
    request.json["instructionId"] = currentInstructionId
    currentInstructionId += 1

    if participantId in list(instructions.keys()):
        instructions[participantId].append(request.json)
    else:
        instructions[participantId] = [request.json]

    return response_success()


@app.route('/participant/<participantId>', methods=['DELETE'])
def delete_participant(participantId):
    if participantId in list(data.keys()):
        data[participantId].visible = False

    return response_success()


@app.route('/participant/<participantId>/hololens/<hololensIp>/battery', methods=['GET'])
def get_hololens_battery(participantId, hololensIp):
    basic = secrets.Secrets.hololensCredentials
    try:
        response = requests.get('http://' + hololensIp + '/api/power/battery', auth=basic, timeout=1).json()
        return response
    except:
        return error_not_found()


@app.route('/participant/<participantId>/hololens/<hololensIp>/thermal', methods=['GET'])
def get_hololens_thermal(participantId, hololensIp):
    basic = secrets.Secrets.hololensCredentials
    try:
        response = requests.get('http://' + hololensIp + '/api/holographic/thermal/stage', auth=basic,
                                timeout=1).json()
        return response
    except:
        return error_not_found()


def check_online_status():
    for participantId, details in data.items():
        if (datetime.datetime.now() - details.last_seen) > datetime.timedelta(seconds=5):
            details.online = False


def participant_seen(participant):
    if participant in data.keys():
        data[participant].online = True
        data[participant].visible = True
        data[participant].last_seen = datetime.datetime.now()


def generate_participant(participantId):
    p = Participant()
    p.participantId = participantId
    data[participantId] = p
    return p


def get_or_generate_participant(participantId):
    if participantId in list(data.keys()):
        # Participant exists
        p = data[participantId]
        assert isinstance(p, Participant)
    else:
        p = generate_participant(participantId)

    if participantId not in list(instructions.keys()):
        instructions[participantId] = []
    return p


def error_server_object():
    return jsonify({"code": 500, "message": "General error."}), 500


def error_not_found():
    return jsonify({"code": 404, "message": "Object not found."}), 404


def response(obj):
    r = Response(jsonpickle.encode(obj, unpicklable=False), 200)
    r.headers['Content-Type'] = 'application/json'
    return r


def response_success():
    return response({"success": True})


def to_class(request_body: dict, class_):
    # first letter to lowercase to handle C# vars
    to_lower = lambda s: s[:1].lower() + s[1:] if s else ''
    keys = list(request_body.keys())

    for key in keys:
        request_body[to_lower(key)] = request_body.pop(key)

    return class_(request_body)


def generate_test_data():
    logging.warning("Loading test data. Remove in production.")

    p = Participant()
    p.participantId = '123456'
    p.online = True
    p.visible = True
    p.needs_help = True
    p.last_seen = datetime.datetime.now()

    e = ExperimentStatus()
    e.participantId = p.participantId
    e.deviceName = "Test Device"
    e.scene = "Test Scene"
    e.timeInCurrentSceneInSeconds = 232.23
    e.fps = 59.9

    etcs = EyeTrackerCalibrationStatus()
    etcs.isEyeCalibrationValid = True
    etcs.isEyeTrackingEnabledAndValid = True
    etcs.offsetX = .02
    etcs.offsetY = .018
    etcs.timestamp = 230.12

    p.experimentStatus = e
    p.eyeTrackerCalibrationStatus = etcs

    data[p.participantId] = p


if __name__ == '__main__':
    # generate_test_data()
    # app.run(host="0.0.0.0")
    http_server = WSGIServer(("0.0.0.0", 5000), app)
    http_server.serve_forever()
