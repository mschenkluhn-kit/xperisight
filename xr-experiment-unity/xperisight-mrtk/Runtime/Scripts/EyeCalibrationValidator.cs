using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit;
using TMPro;
using UnityEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Xperisight
{
    /// <summary>
    /// Check if HoloLens is worn correctly and eye tracker is aligned.
    /// Use with "Calibration" scene.
    /// </summary>
    public class EyeCalibrationValidator : MonoBehaviour
    {

        [SerializeField]
        [Tooltip("Threshold on X-axis when the device is considered to be worn incorrectly.")]
        [Range(0f, .2f)]
        private float thresholdX = .025f;

        [SerializeField]
        [Tooltip("Threshold on Y-axis when the device is considered to be worn incorrectly.")]
        [Range(0f, .2f)]
        private float thresholdY = .025f;

        [SerializeField]
        private bool allowMultipleMeasurements = false;

        [Header("References")]

        [SerializeField]
        private TextMeshPro xText;

        [SerializeField]
        private TextMeshPro yText;

        [SerializeField]
        private TextMeshPro distanceText;

        [SerializeField]
        private TextMeshPro targetText;

        [SerializeField]
        private Color targetColorHover = new Color(188, 95, 95, 255);

        [SerializeField]
        private Color targetColorActivated = new Color(188, 0, 0, 255);

        [Header("Events")]

        [SerializeField]
        [Range(0f, 10f)]
        private float targetFocusDuration = 2.5f;

        [SerializeField]
        private UnityEvent onTargetFocused;

        private float timeOnTarget = 0f;
        private bool targetFocused = false;
        private Color originalTargetColor;

        private List<float> xOffsets = new List<float>();
        private List<float> yOffsets = new List<float>();

        private void Start()
        {
            if (targetText == null)
            {
                this.enabled = false;
                throw new NullReferenceException("Target text not set.");
            }

            originalTargetColor = targetText.color;
        }

        void Update()
        {
            var eyeGazeProvider = CoreServices.InputSystem?.EyeGazeProvider;
            if (eyeGazeProvider != null)
            {
                EyeTrackingTarget lookedAtEyeTarget = EyeTrackingTarget.LookedAtEyeTarget;

                if (lookedAtEyeTarget != null && lookedAtEyeTarget.IsLookedAt)
                {
                    MixedRealityRaycastHit hitInfo = eyeGazeProvider.HitInfo;

                    if (hitInfo.collider != null)
                    {
                        // Calculate difference between hit and center and transform to local coordinates
                        Vector3 distanceFromCenter = hitInfo.transform.InverseTransformDirection(hitInfo.point - hitInfo.collider.bounds.center);

                        if (xText != null && yText != null && distanceText != null && !targetFocused)
                        {
                            xText.text = string.Format("{0:0.000}", distanceFromCenter.x);
                            yText.text = string.Format("{0:0.000}", distanceFromCenter.y);
                            distanceText.text = string.Format("{0:0.000}", distanceFromCenter.magnitude);
                        }

                        if (Math.Abs(distanceFromCenter.x) < thresholdX && Math.Abs(distanceFromCenter.y) < thresholdY)
                        {
                            if (timeOnTarget == 0f)
                            {
                                targetText.color = targetColorHover;
                            }

                            timeOnTarget += Time.deltaTime;

                            // Update statistics
                            xOffsets.Add(distanceFromCenter.x);
                            yOffsets.Add(distanceFromCenter.y);

                            // Termination condition
                            if (!targetFocused && timeOnTarget > targetFocusDuration)
                            {
                                onTargetFocused.Invoke();
                                targetFocused = true;

                                targetText.color = targetColorActivated;

                                EyeTrackerCalibrationStatus etcs = new EyeTrackerCalibrationStatus(xOffsets.Average(), yOffsets.Average(), eyeGazeProvider.IsEyeCalibrationValid.GetValueOrDefault(false), eyeGazeProvider.IsEyeTrackingEnabledAndValid, Time.realtimeSinceStartup, eyeGazeProvider.GazeInputSource.SourceName, eyeGazeProvider.GazeInputSource.SourceType.ToString());

                                // Publish statistics
                                if (ExperimentSupervisor.GetInstance != null)
                                {
                                    StartCoroutine(ExperimentSupervisor.GetInstance.Post("/eye-tracker-calibration-status", etcs));
                                } else
                                {
                                    Debug.LogWarning("ExperimentSupervisor not found. Cannot publish eye tracker calibration status.");
                                }
                                
                            }

                        }
                        else
                        {
                            if (allowMultipleMeasurements || !targetFocused)
                            {
                                timeOnTarget = 0f;
                                targetFocused = false;
                                targetText.color = originalTargetColor;
                                xOffsets.Clear();
                                yOffsets.Clear();
                            }
                        }
                    }
                }
            }
        }
    }
}