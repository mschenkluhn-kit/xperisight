public class EyeTrackerCalibrationStatus
{
    public EyeTrackerCalibrationStatus(float offsetX, float offsetY, bool isEyeCalibrationValid, bool isEyeTrackingEnabledAndValid, float timestamp, string sourceName, string sourceType)
    {
        Timestamp = timestamp;
        OffsetX = offsetX;
        OffsetY = offsetY;
        IsEyeCalibrationValid = isEyeCalibrationValid;
        IsEyeTrackingEnabledAndValid = isEyeTrackingEnabledAndValid;
        SourceName = sourceName;
        SourceType = sourceType;
    }

    public float Timestamp { get; set; }
    public float OffsetX { get; set; }
    public float OffsetY { get; set; }
    public bool IsEyeCalibrationValid { get; set; }
    public bool IsEyeTrackingEnabledAndValid { get; set; }

    public string SourceName { get; set; }
    public string SourceType { get; set; }
}
