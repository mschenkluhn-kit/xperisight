# Xperisight MRTK Addon

## Installation
* Install the base Xperisight package first
* Install the [MRTK feature tool](https://learn.microsoft.com/en-us/windows/mixed-reality/develop/unity/welcome-to-mr-feature-tool) and download MRTK foundation
* Unzip packages
* Open package manager in Unity
* Click "Package from Disk" (or "Package by Name" in older Unity versions) and find the package.json file in the Xperisight package.