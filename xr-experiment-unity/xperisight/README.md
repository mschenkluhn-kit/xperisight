# Xperisight

## Installation
* Unzip packages
* Open package manager in Unity
* Click "Package from Disk" (or "Package by Name" in older Unity versions) and find the package.json file in the Xperisight package.
* In the Unity file explorer, navigate to Packages > Xperisight > Prefabs and drag the Experiment Manager prefab into the scene.
* Configure the ServerIP either by entering the IP in the field or by pressing the load from file button. If loading from file, enter the server IP in a file in Documents > XRExSup > ServerIP.txt