namespace Xperisight
{
    public class SceneAnalytics
    {
        public string Name { get; private set; }

        public SceneAnalytics(string name, string participantId, float timestampEntered)
        {
            Name = name;
            ParticipantId = participantId;
            TimestampEntered = timestampEntered;
        }

        public float TimestampEntered { get; set; }
        public float TimestampLeft { get; set; }
        public float Duration { get => TimestampLeft - TimestampEntered; }
        public string ParticipantId { get; set; }
    }
}