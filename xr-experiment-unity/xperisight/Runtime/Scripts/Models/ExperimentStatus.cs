
namespace Xperisight
{

    /// <summary>
    /// Data Transfer Object to publish data to admin web interface.
    /// </summary>
    public class ExperimentStatus
    {
        public ExperimentStatus(string deviceName, string deviceIp, string deviceModel, string deviceType, string[] sceneNames, string participantId, string scene, float timeInCurrentSceneInSeconds, float fps, bool needsHelp, float deviceBatteryLevel, string deviceBatteryStatus)
        {
            DeviceName = deviceName;
            DeviceIp = deviceIp;
            DeviceModel = deviceModel;
            DeviceType = deviceType;
            SceneNames = sceneNames;
            ParticipantId = participantId;
            Scene = scene;
            TimeInCurrentSceneInSeconds = timeInCurrentSceneInSeconds;
            Fps = fps;
            NeedsHelp = needsHelp;
            DeviceBatteryLevel = deviceBatteryLevel;
            DeviceBatteryStatus = deviceBatteryStatus;
        }

        public string DeviceName { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceType { get; set; }
        public string DeviceIp { get; set; }
        public float DeviceBatteryLevel { get; set; }
        public string DeviceBatteryStatus { get; set; }

        public string[] SceneNames { get; set; }
        public string ParticipantId { get; set; }
        public string Scene { get; set; }
        public float TimeInCurrentSceneInSeconds { get; set; }
        public float Fps { get; set; }
        public bool NeedsHelp { get; set; }
    }
}