using System.Collections.Generic;

namespace Xperisight
{
    public class Instruction
    {
        public Instruction(int instructionId, string call, List<string> parameters)
        {
            InstructionId = instructionId;
            Call = call;
            Parameters = parameters;
        }

        public int InstructionId { get; set; }
        public string Call { get; set; }
        public List<string> Parameters { get; set; }
    }
}