using System;
using System.Threading.Tasks;

#if UNITY_WSA && !UNITY_EDITOR
using Windows.Storage;
#else
using System.IO;
#endif

namespace Xperisight
{
    public class ServerIpStore
    {
        public static async void Save(string ipAddress)
        {
#if UNITY_WSA && !UNITY_EDITOR

        StorageFolder fLocal = ApplicationData.Current.LocalFolder;

        StorageFile file = await fLocal.CreateFileAsync("ServerIP.txt", CreationCollisionOption.OpenIfExists);

        var f = FileIO.WriteTextAsync(file, ipAddress);

        await f;
#else

            // Win32
            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // Create folder if it doesnt exist
            Directory.CreateDirectory(Path.Combine(documents, "XR Experiment Supervisor"));

            var path = Path.Combine(documents, "XR Experiment Supervisor", "ServerIP.txt");

            // Save to file
            File.WriteAllText(path, ipAddress);
#endif
        }

        public static async Task<string> Load()
        {
            string ipAddress = null;

#if UNITY_WSA && !UNITY_EDITOR
            try
            {
                StorageFolder fLocal = ApplicationData.Current.LocalFolder;

                StorageFile file = await fLocal.GetFileAsync("ServerIP.txt");
                if (file != null) {
                    ipAddress = await FileIO.ReadTextAsync(file);
                }
            } catch {
            }
#else

            // Win32
            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // Create folder if it doesnt exist
            Directory.CreateDirectory(Path.Combine(documents, "XR Experiment Supervisor"));

            var path = Path.Combine(documents, "XR Experiment Supervisor", "ServerIP.txt");

            // Read from file
            if (File.Exists(path)) {
                ipAddress = File.ReadAllText(path);
            }
#endif
            if (ipAddress == null || ipAddress.Length == 0) {
                return "http://127.0.0.1:5000";
            } else
            {
                return ipAddress;
            }
        }
    }
}
