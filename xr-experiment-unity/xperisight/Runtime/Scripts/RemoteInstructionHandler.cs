using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#if WINDOWS_UWP
using System;
#endif

namespace Xperisight
{
    public class RemoteInstructionHandler
    {
        private static List<int> acceptedInstructionIds = new List<int>();

        private static bool instructionHandlerRunning = false;

        public static void Handle(List<Instruction> instructions)
        {
            if (instructions != null && instructions.Count > 0)
            {
                foreach (Instruction instruction in instructions)
                {
                    if (!acceptedInstructionIds.Contains(instruction.InstructionId))
                    {
                        acceptedInstructionIds.Add(instruction.InstructionId);
                        RunInstruction(instruction);
                    }
                }
            }
        }

        private static void RunInstruction(Instruction instruction)
        {
            if (!instructionHandlerRunning)
            {
                instructionHandlerRunning = true;

                switch (instruction.Call)
                {
                    case "load_scene":
                        LoadScene(instruction.Parameters[0]);
                        break;
                    case "load_next_scene":
                        LoadNextScene();
                        break;
                    case "load_previous_scene":
                        LoadPreviousScene();
                        break;
                    case "run_eye_calibration":
                        RunEyeCalibration();
                        break;
                    case "eye_tracking_privacy_settings":
                        OpenEyeTrackingPrivacySettings();
                        break;
                    default:
                        Debug.Log($"Unknown: Instruction {instruction.Call} with Parameters {instruction.Parameters}");
                        break;
                }

                instructionHandlerRunning = false;
            }
            else
            {
                Debug.LogWarning("Ignoring remote Instruction to avoid parallel execution.");
            }
        }

        private static void LoadPreviousScene()
        {
            int nextSceneIndex = GetCurrentSceneIndex() - 1;

            if (nextSceneIndex >= 0)
            {
                LoadScene(nextSceneIndex);
            }
            else
            {
                Debug.LogWarning("No next scene found.");
            }
        }

        public static void LoadNextScene()
        {
            int nextSceneIndex = GetCurrentSceneIndex() + 1;

            if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
            {
                LoadScene(nextSceneIndex);
            }
            else
            {
                Debug.LogWarning("No next scene found.");
            }
        }

        private static int GetCurrentSceneIndex()
        {
            int currentSceneIndex;
            if (SceneManager.sceneCount >= 2)
            {
                currentSceneIndex = SceneManager.GetSceneAt(1).buildIndex;
            }
            else
            {
                currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            }

            return currentSceneIndex;
        }

        public static void LoadScene(string scene)
        {
            // Account for a manager scene (MRTK) with additive scene loading
            if (SceneManager.sceneCount >= 2)
            {
                if (scene.Equals(SceneManager.GetSceneAt(0).name))
                {
                    Debug.LogWarning("Scene is already loaded.");
                    return;
                }

                string currentScene = SceneManager.GetSceneAt(1).name;
                SceneManager.UnloadSceneAsync(currentScene);
                SceneManager.LoadScene(scene, LoadSceneMode.Additive);
            }
            else
            {
                SceneManager.LoadScene(scene, LoadSceneMode.Single);
            }
        }

        // Necessary duplicate of LoadScene(string scene) because of Unity's lack of providing to retrieve a scene name by build index
        public static void LoadScene(int sceneIndex)
        {
            // Account for a manager scene (MRTK) with additive scene loading
            if (SceneManager.sceneCount >= 2)
            {
                if (sceneIndex == SceneManager.GetSceneAt(0).buildIndex)
                {
                    Debug.LogWarning("Scene is already loaded.");
                    return;
                }

                string currentScene = SceneManager.GetSceneAt(1).name;
                SceneManager.UnloadSceneAsync(currentScene);
                SceneManager.LoadScene(sceneIndex, LoadSceneMode.Additive);
            }
            else
            {
                SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);
            }
        }


        public static void RunEyeCalibration()
        {
            LaunchUriAsync("ms-hololenssetup://EyeTracking");
        }

        public static void OpenEyeTrackingPrivacySettings()
        {
            LaunchUriAsync("ms-settings:privacy-eyetracker");
        }

        private static async void LaunchUriAsync(string uri)
        {
#if WINDOWS_UWP
    UnityEngine.WSA.Application.InvokeOnUIThread(async () =>
    {
        bool result = await global::Windows.System.Launcher.LaunchUriAsync(new System.Uri(uri));
        if (!result)
        {
            Debug.LogError("Launching URI failed to launch.");
        }
    }, false);
#else
            Debug.Log("Launching URIs not supported outside of Windows UWP.");
#endif
        }
    }
}