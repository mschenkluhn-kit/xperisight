using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Xperisight
{
    public class ExperimentSupervisor : MonoBehaviour
    {

        [SerializeField]
        [Tooltip("e.g., http://127.0.0.1:5000")]
        private string serverUri = "http://127.0.0.1:5000";

        [SerializeField]
        [Tooltip("Server IP can be stored in and loaded from a local text file (Windows: %userprofile%\\documents\\XR Experiment Manager")]
        private bool loadServerUriFromFile = false;

        [SerializeField]
        private float requestInstructionsInterval = 2f;

        [SerializeField]
        private float updateExperimentStateInterval = 1f;

        [SerializeField]
        [Tooltip("Either set a participant ID yourself or the application will automatically set one.")]
        private string participantId;
        public string ParticipantId { get => participantId; set { participantId = value; } }

        private static ExperimentSupervisor instance;
        public static ExperimentSupervisor GetInstance => instance;

        private bool helpButtonPressed;
        public bool HelpButtonPressed { get => helpButtonPressed; set => helpButtonPressed = value; }

        private string deviceName;
        private string deviceModel;
        private string deviceType;
        private SceneAnalytics currentScene;
        private float fps;

        void Start()
        {
            // Ensure singleton is only instanced once
            if (instance == null && instance != this)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Debug.LogWarning("Multiple Experiment Supervisors present in this scene.");
                GetComponent<ExperimentSupervisor>().enabled = false;
                return;
            }

            if (loadServerUriFromFile)
            {
                LoadAndSaveIpAddressFromStorage();
            }

            // Generate a participant ID if one is not set
            if (participantId == null || participantId == "")
            {
                participantId = Guid.NewGuid().ToString();
            }

            currentScene = new SceneAnalytics(SceneManager.GetActiveScene().name, participantId, Time.realtimeSinceStartup);

            deviceName = SystemInfo.deviceName;
            deviceModel = SystemInfo.deviceModel;
            deviceType = SystemInfo.deviceType.ToString();

            StartCoroutine(UpdateExperimentStateLoop());
            StartCoroutine(GetInstructionsLoop());
        }

        private async void LoadAndSaveIpAddressFromStorage()
        {
            serverUri = await ServerIpStore.Load();
            ServerIpStore.Save(serverUri);
        }

        private void Update()
        {
            // Update FPS with smoothing
            fps = Mathf.Lerp(fps, 1 / Time.deltaTime, 0.2f);
        }

        private IEnumerator UpdateExperimentStateLoop()
        {
            while (true)
            {
                yield return new WaitForSeconds(updateExperimentStateInterval);
                UpdateExperimentState();
            }
        }

        private void UpdateExperimentState()
        {
            UpdateSceneInformation();

            float timeInScene = currentScene is null ? Time.realtimeSinceStartup : Time.realtimeSinceStartup - currentScene.TimestampEntered;

            // Generate list of scenes names from SceneManager
            string[] sceneNames = GetSceneNames();

            ExperimentStatus status = new ExperimentStatus(deviceName, GetLocalIPv4(), deviceModel, deviceType, sceneNames, participantId, currentScene.Name, timeInScene, fps, HelpButtonPressed, SystemInfo.batteryLevel, SystemInfo.batteryStatus.ToString());

            StartCoroutine(Post("/experiment-status", status));
        }

        private static string[] GetSceneNames()
        {
            string[] sceneNames = new string[SceneManager.sceneCountInBuildSettings];
            for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
            {
                // Get scene names from Build Index https://discussions.unity.com/t/how-to-get-scene-name-at-certain-buildindex/175723/3
                string path = SceneUtility.GetScenePathByBuildIndex(i);
                int slash = path.LastIndexOf('/');
                string name = path.Substring(slash + 1);
                int dot = name.LastIndexOf('.');
                name = name.Substring(0, dot);

                if (name != null && name.Length > 0)
                {
                    sceneNames[i] = name;
                }
            }

            return sceneNames;
        }

        private void UpdateSceneInformation()
        {
            // Differentiate Manager Scene (MRTK) from single scene mode
            if (SceneManager.sceneCount >= 2)
            {
                // Additive scene loading

                // Update currentScene
                Scene scene = SceneManager.GetSceneAt(1);
                if (scene.name != currentScene.Name)
                {
                    currentScene = new SceneAnalytics(scene.name, participantId, Time.realtimeSinceStartup);
                }

            }
            else
            {
                // Single scene

                // Update currentScene
                Scene scene = SceneManager.GetSceneAt(0);
                if (scene.name != currentScene.Name)
                {
                    currentScene = new SceneAnalytics(scene.name, participantId, Time.realtimeSinceStartup);
                }
            }
        }




        private IEnumerator GetInstructionsLoop()
        {
            while (true)
            {
                yield return new WaitForSeconds(requestInstructionsInterval);
                yield return StartCoroutine(GetInstructions());
            }
        }

        public IEnumerator Post(string path, object serializable)
        {
            byte[] payload = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(serializable));

            var uwr = new UnityWebRequest(serverUri + "/participant/" + participantId + path, "POST");
            uwr.timeout = 1;
            uwr.SetRequestHeader("Content-Type", "application/json");

            uwr.uploadHandler = new UploadHandlerRaw(payload);

            yield return uwr.SendWebRequest();

            if (uwr.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.LogWarning("Connection error.");
                yield return new WaitForSeconds(5);
            }
        }

        /// <summary>
        /// Listen for instructions from the web server.
        /// </summary>
        private IEnumerator GetInstructions()
        {
            using UnityWebRequest request = UnityWebRequest.Get(serverUri + "/participant/" + participantId + "/instructions");
            request.timeout = 1;
            yield return request.SendWebRequest();

            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError("Error: " + request.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    // Debug.LogError("HTTP Error: " + request.error);
                    break;
                case UnityWebRequest.Result.Success:
                    List<Instruction> instructions = JsonConvert.DeserializeObject<List<Instruction>>(request.downloadHandler.text);
                    RemoteInstructionHandler.Handle(instructions);
                    break;
            }

        }


        internal static void HandleHelpButtonPressed()
        {

        }

        internal static void HandleHelpButtonReleased()
        {
            throw new NotImplementedException();
        }


        #region Helpers

        public static string GetLocalIPv4()
        {
            string ipv4 = "";
            try
            {
                ipv4 = Dns.GetHostEntry(Dns.GetHostName())
                 .AddressList.First(
                     f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                 .ToString();
            }
            catch (Exception)
            {
                Debug.Log("Cannot retrieve IPv4 address.");
            }
            return ipv4;
        }

        #endregion
    }
}
