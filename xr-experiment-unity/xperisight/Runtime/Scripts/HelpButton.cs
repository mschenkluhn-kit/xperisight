using UnityEngine;

namespace Xperisight
{
    public class HelpButton : MonoBehaviour
    {
        private static HelpButton instance;

        private void Start()
        {
            if (instance == null && instance != this)
            {
                instance = this;
            }
            else
            {
                Debug.LogWarning("Multiple Help Buttons present in this scene.");
            }

            if (ExperimentSupervisor.GetInstance == null)
            {
                Debug.LogWarning("Experiment Supervisor not found in scene.");
            }
        }

        public void Press()
        {
            if (ExperimentSupervisor.GetInstance != null)
            {
                ExperimentSupervisor.GetInstance.HelpButtonPressed = true;
            }

        }

        public void Release()
        {
            if (ExperimentSupervisor.GetInstance != null)
            {
                ExperimentSupervisor.GetInstance.HelpButtonPressed = false;
            }
        }

        public void Toggle()
        {
            if (ExperimentSupervisor.GetInstance != null)
            {
                ExperimentSupervisor.GetInstance.HelpButtonPressed = !ExperimentSupervisor.GetInstance.HelpButtonPressed;
            }
        }
    }
}
