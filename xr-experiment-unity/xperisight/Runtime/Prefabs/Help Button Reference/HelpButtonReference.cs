using UnityEngine;

namespace Xperisight
{
    public class HelpButtonReference : MonoBehaviour
    {
        private bool isTriggered = false;

        [Header("This help button demonstration does not require additional libraries.")]
        [Header("For improved UX, please implement this button based on your prefered library (MRTK, Meta Quest Interaction SDK, ...)")]
        [SerializeField]
        private bool isPressed = false;

        [SerializeField]
        private HelpButton helpButton;

        [SerializeField]
        private Material unpressedMat;

        [SerializeField]
        private Material pressedMat;

        private void Start()
        {
            GetComponent<Renderer>().material = unpressedMat;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!isTriggered)
            {
                if (!isPressed)
                {
                    isPressed = true;
                    helpButton.Press();
                    GetComponent<Renderer>().material = pressedMat;
                }
                else
                {
                    isPressed = false;
                    helpButton.Release();
                    GetComponent<Renderer>().material = unpressedMat;
                }


            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (isTriggered)
            {
                isTriggered = false;
            }
        }

    }
}